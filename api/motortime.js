import request from "@/utils/request.js";
let source, platform, appname

uni.getSystemInfo({
	success: function(res) {
		console.log(res)
		// #ifdef APP-PLUS
		platform = res.platform
		appname = res.platform
		if (res.platform == 'ios') {
			source = 6
		} else {
			source = 8
		}
		// #endif
		// #ifdef MP-WEIXIN
		appname = 'weixin'
		platform = 1
		source = 9
		// #endif
		// #ifdef MP-TOUTIAO
		appname = 'toutiao'
		platform = 2
		source = 17 + '-' + appName
		// #endif
		// #ifdef MP-BAIDU
		appname = 'baidu'
		platform = 3
		source = 18 + '-' + appName
		// #endif
	}
});
// uni.getProvider({
// 	service: 'oauth',
// 	success: function(res) {
// 		var provider = res.provider.toString()
// 		if (provider == 'weixin') {
// 			source = 9
// 			platform = 1
// 			appname = 'weixin'
// 		} else if (provider == 'toutiao') {
// 			source = 17
// 			platform = 2
// 			uni.getSystemInfo({
// 				success: function(res) {
// 					console.log(res)
// 					appname = res.appName
// 					source = 17 + '-' + res.appName
// 				}
// 			});
// 		} else if (provider == 'baidu') {
// 			platform = 3
// 			uni.getSystemInfo({
// 				success: function(res) {
// 					console.log(res)
// 					appname = res.host
// 					source = 18 + '-' + res.appName
// 				}
// 			});
// 		}


// 	}
// });
//埋点
export function burialPoint(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	data.invitees_scene_id = uni.getStorageSync('Appoptions') ? uni.getStorageSync('Appoptions').scene : ''
	data.equipment_id = uni.getStorageSync('equipment_id') ? uni.getStorageSync('equipment_id') : 0
	return request.post('user/burial_point', data);
}
//分享任务
export function getShareTask(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('share-task', data);
}
//首页轮播
export function getAdvs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('advs', data);
}
//滚动通知
export function getNoticeList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/index', data);
}
//分享图片
export function getShareImg(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('share-info', data);
}
//首页置顶内容
export function getTopList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/lists', data);
}
//发帖
export function postBbs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card', data);
}
//帖子列表
export function getBbsList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/cards', data);
}
//检测是否禁言
export function checkText(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('check-text', data);
}
//文章评论
export function articleCommentpost(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/comment', data);
}
//帖子评论
export function bbsCommentpost(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card-comment', data);
}
//大v列表
export function getRecommend(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('user/recommend-user', data);
}
//关注/取消关注
export function followUser(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/follow', data);
}
//话题列表
export function getTopicList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/lists', data);
}
//话题详情
export function getTopicInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/' + id, data);
}
//话题详情帖子列表
export function getTopicBbs(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('topic/cards', data);
}
//资讯分类
export function getChannel(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/media/channel', data);
}
//资讯列表

export function getArticlelist(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('home/lists', data);
}
//投票
export function voteChoose(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('vote/choose', data);
}
//投票详情
export function voteInfo(id) {
	return request.post('vote/' + id);
}
export function getDemo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('demo', data);
}
export function postDemo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('demo', data);
}

//资讯详情
export function getArticleInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id, data);
}
//资讯为您推荐
export function getRecommends(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id + '/recommends', data);
}
//资讯评论列表
export function getCommentlist(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/' + id + '/comments', data);
}
//资讯点赞
export function articlePraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/praise', data);
}
//资讯一般般
export function articlenoPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/praise-no', data);
}
//资讯收藏
export function articleCollect(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/collect', data);
}

//资讯评论点赞
export function articleCommPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('archive/comment/praise', data);
}
//删除资讯评论
export function articleCommDel(id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.delete('archive-comment/' + id + '/delete');
}
//资讯评论详情
export function articleCommInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive-comment/' + id, data);
}
//资讯评论详情回复列表
export function articleCommReplys(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive-comment/' + id + '/replys', data);
}
//资讯生成海报
export function articlePoster(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('archive/share-poster', data);
}
//帖子生成海报
export function bbsPoster(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/share-poster', data);
}
//帖子详情
export function getBbsInfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/card/' + id, data);
}
//帖子评论列表
export function getBbsCommentlist(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('bbs/card/' + id + '/comments', data);
}
//帖子点赞
export function bbsPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card/praise', data);
}
//帖子收藏
export function bbsCollect(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card/collect', data);
}
//帖子/评论举报
export function bbsTip(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/tip', data);
}
//帖子删除
export function delBbs(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.delete('bbs/card/' + id + '/delete', data);
}
//帖子评论点赞
export function bbsCommentPraise(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('bbs/card-comment/praise', data);
}
//帖子评论删除
export function bbsDelComment(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.delete('bbs/card-comment/' + id + '/delete', data);
}

//登录
export function motorLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('mini-login-new', data);
}
//app微信登录
export function motorappLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('app-new-login', data);
}
//app苹果登录
export function motorappAppleLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/ios-login', data);
}

//获取验证码
export function getCode(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('sms/code', data);
}
//手机号登录
export function motorphoneLogin(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/login', data);
}
//绑定手机号
export function phoneBinding(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('user/phone_binding', data);
}
//绑定微信号
export function wechatBinding(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('app-weixin-bingding', data);
}
//用户信息
export function getMemberInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('user', data);
}
//用户信息
export function getUserinfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.get('user/' + id, data);
}

//保存用户信息
export function saveMemberInfo(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('user', data);
}
//用户基础信息
export function getEquipment(data) {
	return request.post('user/equipment', data);
}
//个人首页列表
export function getUserList(id, data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('space/' + id + '/lists', data);
}
//消息数量
export function getNoticenums(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/nums', data);
}
//评论消息
export function getCommentMessageList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/comments', data);
}
//系统消息
export function getAdminMessageList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/system', data);
}
//消息已读
export function readMessage(url) {
	return request.put(url);
}
//我的活动
export function getMyActive(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-lists', data);
}
//商品列表
export function getShopList(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/goods', data);
}
//商品分类列表
export function getShopColumns(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('shop/columns', data);
}
//直播详情
export function getLiveInfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.get('live/' + id, data);
}
//直播状态
export function getLiveStatus(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source,
	}
	return request.get('live/status/' + id, data);
}
//直播评论列表
export function getLiveComments(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('live/' + id + '/comments', data);
}
//直播评论
export function postLive(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('live/comment', data);
}
//模板消息
export function noticeTemplate(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('notice/template', data);
}
//获取我的车辆列表
export function getMyCarlist(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/list', data);
}
//删除车辆
export function delCars(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.delete('car/' + id + '/delete', data);
}
//设为默认
export function defultCars(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.put('car/' + id + '/default', data);
}
//获取车辆品牌
export function getBrands() {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('car/brands', data);
}
//获取车系
export function selectCar(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/cars', data);
}
//获取代系
export function selectGenera(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/generations', data);
}

//获取车型
export function selectModel(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('car/models', data);
}
//获取车详情
export function getCarinfo(id) {
	var data = {
		platform: platform,
		appname: appname,
		source: source
	}
	return request.get('car/' + id, data);
}
//添加车辆认证
export function submitCars(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.post('car/add', data);
}
//修改车辆认证  
export function editCars(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.put('car/' + id + '/update', data);
}
//地区列表
export function getAreaList(data) {
	return request.get('shop/areas-all', data);
}
//活动详情
export function getActiveinfo(data, id) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/' + id + '/recruit-show', data);
}
//获取礼品列表
export function getGiftlist(data) {
	data.platform = platform
	data.appname = appname
	data.source = source
	return request.get('huodong/recruit-prize-my-record', data);
}
